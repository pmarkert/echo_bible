var idx = require("../index");
var should = require("should");

describe("Test Script", function() {
	it("Should run", function(done) {
		var request = require("./scripts/verse_lookup.request.json");
		var expected = require("./scripts/verse_lookup.response.json");
		context = { succeed : function(actual) { 
			actual.should.equal(expected);
			done();
		}};
		idx.handler(request, context);	
	});
});
