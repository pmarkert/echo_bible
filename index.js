var http = require("http");
var alexa = require("alexa_router");

var router = new alexa.Router();
router.add("VerseLookup", verse_lookup);
router.add("VerseOfTheDay", verse_of_the_day);
router.add(verse_of_the_day); // For the launch action
exports.handler = router.handler;

function verse_lookup(params, session, callback) {
	params.Verse=params.Verse.replace(' through ', '-');
	params.Book=params.Book.replace('first ', '1+').replace('second ', '2+').replace('third ', '3+');
	console.log('Retrieving verse: ' + params.Book + ' ' + params.Chapter + ':' + params.Verse);

	http.get("http://labs.bible.org/api/?passage=" + params.Book + "+" + params.Chapter + ":" + params.Verse + "&formatting=plain", function(res) {
		res.on('data', function(data) {
			console.log("Bible Verse response from service - " + data);
			callback(data.toString(), "Verse Lookup - " + params.Book + " " + params.Chapter + ":" + params.Verse);
		});
	});
}

function verse_of_the_day(params, session, callback) {
	// If we wanted to initialize the session to have some attributes we could add those here.
	console.log("Retrieving verse of the day...");
	http.get("http://labs.bible.org/api/?passage=votd&type=json", function(res) {
		res.on('data', function(data) {
			console.log("Bible Verse of the Day response from service - " + data);
			var response = JSON.parse(data);
			var speechOutput = "The verse of the day is " + response[0].bookname + 
				", chapter " + response[0].chapter + 
				", verse " + response[0].verse + 
				". " + response[0].text;
			callback(speechOutput, "Verse of the Day");
		});
	});
}
